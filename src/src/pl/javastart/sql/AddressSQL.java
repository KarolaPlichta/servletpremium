package src.pl.javastart.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pl.javastart.domain.Address;
public class AddressSQL {
	
	private Statement statement;
	private Connection connection;
	private PreparedStatement insertAddressStmt;
	private PreparedStatement updateAddressStmt;
	
	public AddressSQL(Connection connection){
		this.setConnection(connection);
		try {
			statement = connection.createStatement();
			this.insertAddressStmt = connection.prepareStatement("INSERT INTO `servlet`.`address`(`idaddress`,`typAdresu`,`województwo`,`miasto`,`kodPocztowy`,`ulica`,`numer D/M`,`iduser`) VALUES (NULL,?,?,?,?,?,?,?);");
			this.updateAddressStmt = connection.prepareStatement("UPDATE `servlet`.`address` SET `typAdresu` = ?, `województwo` = ?, `miasto` = ?, `kodPocztowy` = ?, `ulica` = ?, `numer D/M` = ? WHERE `idaddress` = ?;");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean add(Address address, int id){
		boolean x = false;
		try {
			insertAddressStmt.setString(1, address.getType());
			insertAddressStmt.setString(2, address.getRegion());
			insertAddressStmt.setString(3, address.getCity());
			insertAddressStmt.setString(4, address.getZipcode());
			insertAddressStmt.setString(5, address.getStreet());
			insertAddressStmt.setString(6, address.getNumber());
			insertAddressStmt.setInt(7, id);
			insertAddressStmt.execute();
			x = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return x;
	}
	
	public boolean update(Address address, String id){
		boolean x = false;
		try {
			updateAddressStmt.setString(1, address.getType());
			updateAddressStmt.setString(2, address.getRegion());
			updateAddressStmt.setString(3, address.getCity());
			updateAddressStmt.setString(4, address.getZipcode());
			updateAddressStmt.setString(5, address.getStreet());
			updateAddressStmt.setString(6, address.getNumber());
			updateAddressStmt.setString(7, id);
			updateAddressStmt.execute();
			x = true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return x;
	}
	
	public boolean delete(String id){
		boolean x = false;
		try {
			statement.execute("DELETE FROM `servlet`.`address` WHERE `idaddress` = '" + id + "';");
			x = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return x;
	}
	
	public List<Address> selectAll(int id){
		List<Address> addresses = new ArrayList<Address>();
		try {
			ResultSet result = statement.executeQuery("SELECT `address`.`idaddress`, `address`.`typAdresu`, `address`.`województwo`,`address`.`miasto`, `address`.`kodPocztowy`, `address`.`ulica`, `address`.`numer D/M` FROM `servlet`.`address`WHERE `address`.`iduser`='" +id+ "' ;");
			while(result.next()){
				Address address = new Address();
				address.setId(result.getInt(1));
				address.setType(result.getString(2));
				address.setRegion(result.getString(3));
				address.setCity(result.getString(4));
				address.setZipcode(result.getString(5));
				address.setStreet(result.getString(6));
				address.setNumber(result.getString(7));
				addresses.add(address);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return addresses;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

}
