package src.pl.javastart.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pl.javastart.domain.Status;
import pl.javastart.domain.User;

public class UsersSQL {
	

	private Statement statement;
	private Connection connection;
	private PreparedStatement insertUserStmt;
	private PreparedStatement selectAllUserStmt;

	public UsersSQL(Connection connection2){
		this.setConnection(connection2);
		try {
			statement = connection.createStatement();
			this.insertUserStmt = connection2.prepareStatement("INSERT INTO `servlet`.`user`(`iduser`, `username`, `password`, `email`, `status` ) VALUES(NULL,?,?,?,?);");

			selectAllUserStmt = connection2.prepareStatement("SELECT *FROM `servlet`.`user`;");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	public void add(User user) throws IOException{
	

		try {
			
				if(user.getConfirmPassword()){
					insertUserStmt.setString(1, user.getUsername());
					insertUserStmt.setString(2, user.getPassword());
					insertUserStmt.setString(3, user.getEmail());
					insertUserStmt.setString(4, String.valueOf(user.getStatus()));
					insertUserStmt.execute();
					
				}
				
		}	
		 catch (SQLException e){
			e.printStackTrace();
		}
		}
	
	public void update(String username, String status){
		try {
			statement.execute("UPDATE `servlet`.`user` SET `status` = '" + status + "' WHERE `username` = '" + username + "';");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(String username){
		try {
			statement.execute("DELETE FROM `servlet`.`user` WHERE `username` = '" + username + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<User> selectAll(){
		List<User> users = new ArrayList<User>();
		try {
			 ResultSet result = selectAllUserStmt.executeQuery();
	            while(result.next()){
	            	User user = new User();
	            	user.setUsername(result.getString("username"));
	            	user.setPassword(result.getString("password"));
	            	user.setEmail(result.getString("email"));
	            	user.setStatus((Status) result.getObject("status"));
	            	users.add(user);
	            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}

	public boolean selectByUsername(String username){
		boolean x = false;
		try {
				x= statement.execute("SELECT *FROM `servlet`.`user` WHERE `username` Like '%" + username + "%';");
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return x;
	}

	public boolean selectByUsernameAndPassword(String username, String password){
		boolean x = false;
		try {
				
				x = statement.execute("SELECT *FROM `servlet`.`user` WHERE `username` Like '%" + username + "%' And `password` Like '%" + password + "%';");
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return x;
	}
	
	

	public Connection getConnection() {
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

}
