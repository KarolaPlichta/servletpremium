package pl.javastart.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import src.pl.javastart.sql.UsersSQL;

public class ChangePermission extends HttpServlet {

	
	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String username = request.getParameter("username");
		String action = request.getParameter("action");
		String status = request.getParameter("status");
		String URL = "jdbc:mysql://127.0.0.1:3306/servlet?user=root&password=PANtarei#123";
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2>Result</h2>");
		
		try {
			Connection connection = DriverManager.getConnection(URL);
			UsersSQL sql = new UsersSQL(connection);
			if(action.equals("delete")){
				sql.delete(username);
				connection.close();
				out.println("<h4>U�ytkownik zosta� usuni�ty.</h4>");
			}
			else if(action.equals("add")){
				sql.update(username, status);
				connection.close();
				out.println("<h4>Uprawnienia u�ytkownika zosta�y zmienione.</h4>");

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		out.println("</body></html>");
		out.close();
		
	}

}
