package pl.javastart.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.javastart.domain.Address;
import src.pl.javastart.sql.AddressSQL;

public class EditAddress extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String URL = "jdbc:mysql://127.0.0.1:3306/servlet?user=root&password=PANtarei#123";
		String type = request.getParameter("type");
		String region = request.getParameter("region");
		String city = request.getParameter("city");
		String zipcode = request.getParameter("zipcode");
		String street = request.getParameter("street");
		String number = request.getParameter("number");
		String id =(String)request.getSession().getAttribute("addressid");
	
		Address address = new Address(type, region, city, zipcode, street, number);
		PrintWriter out = response.getWriter();
		out.println(id);
		try {
			Connection connection = DriverManager.getConnection(URL);
			AddressSQL sql = new AddressSQL(connection);
			if(sql.update(address, id)){
				connection.close();
				response.sendRedirect("succesful.jsp");
			}
			else{
				connection.close();
				response.sendRedirect("failed.jsp");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	
		
	}

}
