package pl.javastart.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import src.pl.javastart.sql.AddressSQL;

public class DeleteAddress extends HttpServlet{

	private static final long serialVersionUID = 1L;
	String id;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String URL = "jdbc:mysql://127.0.0.1:3306/servlet?user=root&password=PANtarei#123";
		
		
		try {
			Connection connection = DriverManager.getConnection(URL);
			AddressSQL sql = new AddressSQL(connection);
			if(sql.delete(id)){
				connection.close();
				response.sendRedirect("succesful.jsp");
			}
			else{
				connection.close();
				response.sendRedirect("failed.jsp");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		id =(String)request.getSession().getAttribute("addressid");
		doPost(request, response);
	}
}
