package pl.javastart.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.javastart.domain.Address;
import src.pl.javastart.sql.AddressSQL;

public class Profile extends HttpServlet{

	private static final long serialVersionUID = 1L;
	private String text;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String URL = "jdbc:mysql://127.0.0.1:3306/servlet?user=root&password=PANtarei#123";
		Statement statement;

		response.setContentType("text/html");
		String username = (String) request.getSession().getAttribute("username");
		int id = 0;
		text ="<h3>Twoje adresy to: </h3><br><br>";
		
		Connection connection;
		try {
			connection = DriverManager.getConnection(URL);
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery("Select iduser From servlet.user where username = '" + username + "';");
			
			while(result.next()){
				id = result.getInt(1);
			}
			request.getSession().setAttribute("userid", id);
			
			AddressSQL sql = new AddressSQL(connection);
			
			if(id==0){
				text = "Error. No user selected.";
			}
			else{	
				int i = 1;
				for(Address current: sql.selectAll(id)){
					i = current.getId();
					text+="<b><li>" + current.getType() + ":</b> " + current.getRegion() + ", " + current.getZipcode() + ", " + current.getCity() + ", " +
						current.getStreet() + ", " + current.getNumber() + " <a href='editAddress.jsp?addressid="+ i +"'>Edytuj adres</a> <a href='/Servlet2/DeleteAddress?addressid="+ i +"'>Usu� adres</a></li><br>";
							
				}
			}
			
					
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println(text);
		request.getSession().setAttribute("lista", text);
		response.sendRedirect("profile.jsp");
		
		
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		doPost(request, response);
	}


}
