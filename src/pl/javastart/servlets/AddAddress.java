package pl.javastart.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.javastart.domain.Address;
import src.pl.javastart.sql.AddressSQL;

public class AddAddress extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String URL = "jdbc:mysql://127.0.0.1:3306/servlet?user=root&password=PANtarei#123";
		String type = request.getParameter("type");
		String region = request.getParameter("region");
		String city = request.getParameter("city");
		String zipcode = request.getParameter("zipcode");
		String street = request.getParameter("street");
		String number = request.getParameter("number");
		Address address = new Address(type, region, city, zipcode, street, number);
		int id = (int) request.getSession().getAttribute("userid");
		
		try {
			Connection connection = DriverManager.getConnection(URL);
			AddressSQL sql = new AddressSQL(connection);
			if(sql.add(address, id)){
				response.sendRedirect("succesful.jsp");
			}
			else{
				response.sendRedirect("failed.jsp");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		doPost(request, response);
	}

}
