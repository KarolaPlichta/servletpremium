package pl.javastart.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import src.pl.javastart.sql.UsersSQL;

public class Login extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
	
		String URL = "jdbc:mysql://127.0.0.1:3306/servlet?user=root&password=PANtarei#123";
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		request.getSession().setAttribute("username", username);
		
		
	try {
			
			
			Connection connection = DriverManager.getConnection(URL);
			UsersSQL sql = new UsersSQL(connection);
			if(sql.selectByUsername(username)){
				if(sql.selectByUsernameAndPassword(username, password)){
					response.sendRedirect("/Servlet2/Profile");
			}
				else{
					response.sendRedirect("wrongPassword.jsp");
				}
			}
			else {
				connection.close();
				response.sendRedirect("notExist.jsp");
			}
			
			
		} catch (SQLException e) {
	
			e.printStackTrace();
		}
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException{
		doPost(request, response);
	}
	
	

}
