package pl.javastart.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.javastart.domain.Status;
import pl.javastart.domain.User;
import src.pl.javastart.sql.UsersSQL;

public class Register extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("password2");
		String email = request.getParameter("email");
		
		String URL = "jdbc:mysql://127.0.0.1:3306/servlet?user=root&password=PANtarei#123";
		
		User user = new User(username, password, confirmPassword, email, Status.standard);
		
	try {
			
			
			Connection connection = DriverManager.getConnection(URL);
			UsersSQL sql = new UsersSQL(connection);
			
			if(user.getStatus().equals(Status.notExist)){
				connection.close();
				response.sendRedirect("exist.jsp");
			}
			else{
				sql.add(user);
				connection.close();
				response.sendRedirect("good.jsp");
			}
			
			
			
		} catch (SQLException e) {
	
			e.printStackTrace();
		}
		
	
		
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		doPost(request,response);
	}

}
