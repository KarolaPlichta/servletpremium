package pl.javastart.domain;

public class User {
	
	private String username;
	private String password;
	private boolean confirmPassword;
	private String email;
	private Status status;
	
	
	public User(){
		super();
	}
	
	public User(String username, String password, String email, Status status){
		this.setUsername(username);
		this.setPassword(password);
		this.setConfirmPassword(password);
		this.setEmail(email);
		this.setStatus(status);
	}
	
	public User(String username, String password, String confirmPassword, String email, Status status){
		this.username=username;
		this.setPassword(password);
		this.setConfirmPassword(confirmPassword);
		this.setEmail(email);
		this.setStatus(status);
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String truePassword) {
		this.confirmPassword = this.password.equals(truePassword);
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
